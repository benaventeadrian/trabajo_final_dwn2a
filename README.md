## E-Wallet ##

Proyecto para el segundo cuatrimestre de la carrera de Diseño Web, de Primera Escuela de Arte Multimedial Da Vinci. Trabajo que surge de la articulación entre las materias Programación I, Interacción con Dispositivos Móviles, Diseño Gráfico para Web y Diseño Vectorial.

Se trata de una app mobile híbrida de e-commerce y monedero digital de BitCoin, con una landing page promocional y una campaña publicitaria urbana.

En la materia **Programación I** se implementó la funcionalidad del carrito de compras, en **IDM** se maquetaron las secciones, la landing page utilizando el efecto parallax (librería skrollr.js) y se empaquetó un instalable (APK) para poder ejecutar la aplicación en dispositivos Android. Por otra parte, en **Diseño Gráfico par Web** se desarrollaron los prototipos de las vistas y en la materia **Diseño Vectorial** se crearon el logo e identidad de la marca, y se realizaron mockups de la aplicación en distintos escenarios de uso, así como un afiche publicitario presentado en distintos soportes de comunicación (carteles, revistas, folletos, etc.).

- **Repositiorio de la app**: https://gitlab.com/benaventeadrian/e-wallet.git

- **Landing page**: https://benaventeadrian.gitlab.io/ewallet-landing
- **Repositiorio de la landing**: https://gitlab.com/benaventeadrian/ewallet-landing.git

